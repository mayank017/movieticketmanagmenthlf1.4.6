/*
 * SPDX-License-Identifier: Apache-2.0
 */

// peer chaincode invoke -C mychannel -n movieticket1 -o orderer1.com:7050  --tls  --cafile /etc/hyperledger/orderer/msp/tlsca/tlsca.orderer-cert.pem  -c '{"function":"addTheater","Args":["THEATER1", "MTH", "3" , "5"]}'

// peer chaincode invoke -C mychannel -n movieticket1 -o orderer1.com:7050  --tls  --cafile /etc/hyperledger/orderer/msp/tlsca/tlsca.orderer-cert.pem  -c '{"function":"addMovie" ,"Args":["MOVIEID1", "MOVIE1", "THEATER1" , "4"]}'

//peer chaincode query -C mychannel -n movieticket1 -o orderer1.com:7050   --tls  --cafile /etc/hyperledger/orderer/msp/tlsca/tlsca.orderer-cert.pem -c '{"function":"getMovie","Args":["MOVIEID1"]}'

//peer chaincode query -C mychannel -n movieticket1 -o orderer1.com:7050   --tls  --cafile /etc/hyperledger/orderer/msp/tlsca/tlsca.orderer-cert.pem -c '{"function":"getTheater","Args":["THEATE11R1"]}' 

//peer chaincode invoke -C mychannel -n movieticket1 -o orderer1.com:7050  --tls  --cafile /etc/hyperledger/orderer/msp/tlsca/tlsca.orderer-cert.pem  -c '{"function":"addShowforMovie" ,"Args":["SHOWID3", "1614508322","MOVIEID1", "10","MORNING_SHOW" ]}' 

//peer chaincode query -C mychannel -n movieticket1 -o orderer1.com:7050   --tls  --cafile /etc/hyperledger/orderer/msp/tlsca/tlsca.orderer-cert.pem -c '{"function":"getShow","Args":["SHOWID3"]}'  

//peer chaincode invoke -C mychannel -n movieticket1 -o orderer1.com:7050  --tls  --cafile /etc/hyperledger/orderer/msp/tlsca/tlsca.orderer-cert.pem  -c '{"function":"purchaseTickets" , "Args":["P12","W3","THEATER1","999","3" ,"SHOWID4"]}'

//peer chaincode invoke -C mychannel -n movieticket1 -o orderer1.com:7050  --tls  --cafile /etc/hyperledger/orderer/msp/tlsca/tlsca.orderer-cert.pem  -c '{"function":"addCafeteria" , "Args":["CAF123","THEATER1"]}'

//peer chaincode query -C mychannel -n movieticket1 -o orderer1.com:7050   --tls  --cafile /etc/hyperledger/orderer/msp/tlsca/tlsca.orderer-cert.pem -c '{"function":"getCafeteriaDetails","Args":["CAF123"]}' 

//peer chaincode invoke -C mychannel -n movieticket1 -o orderer1.com:7050  --tls  --cafile /etc/hyperledger/orderer/msp/tlsca/tlsca.orderer-cert.pem  -c '{"function":"collectFreeBie" , "Args":["P12"]}'

//peer chaincode invoke -C mychannel -n movieticket1 -o orderer1.com:7050  --tls  --cafile /etc/hyperledger/orderer/msp/tlsca/tlsca.orderer-cert.pem  -c '{"function":"applyBottleforExchage" , "Args":["P13"]}'

'use strict';

const {
    Contract
} = require('fabric-contract-api');
// const ClientIdentity = require('fabric-shim').ClientIdentity;

class FabCar extends Contract {

    async initLedger(ctx) {
        console.info('============= START : Initialize Ledger ===========');
        const infos = [{
            id: 'userId',
            hash: 'vaccineHash'
        }];

        for (let i = 0; i < infos.length; i++) {
            // infos[i].docType = 'car';
            await ctx.stub.putState(infos[i]['id'], Buffer.from(JSON.stringify(infos[i])));
            console.info('Added <--> ', infos[i]);
        }
        console.info('============= END : Initialize Ledger ===========');
    }
  
    // async queryId(ctx, id) {
    //     if (id.length <= 0) {
    //         throw new Error(`Please provide id param!`);
    //     }
    //     let cid = new ClientIdentity(ctx.stub); // "stub" is the ChaincodeStub object passed to Init() and Invoke() methods
    //     var callerMspid = cid.getMSPID()
    //     console.info("callerMspid--MSPID--", callerMspid);
    //     console.info("-------########################################################---------------");
    //     var callerid = cid.getID()
    //     console.info("callerid", callerid)
    //     console.info("-------########################################################---------------");
    //     var callerCertificate = cid.getX509Certificate()
    //     console.info("callerCertificate", callerCertificate)
    //     console.info("-------########################################################---------------");
    //     const dataAsBytes = await ctx.stub.getState(id); // get the car from chaincode state
    //     if (!dataAsBytes || dataAsBytes.length === 0) {
    //         throw new Error(`${id} does not exist`);
    //     }
    //     console.info(dataAsBytes.toString());
    //     return dataAsBytes.toString();
    // }
        
    async getpsudonumber(ctx){ 
        let no = await  ctx.stub.getTxTimestamp()
        console.info(" ctx.stub.getTxTimestamp()",no.nanos)
       return await no.nanos;
    }

    async getTheater(ctx, id) {
        if (id.length <= 0) {
            throw new Error(`Please provide id param!`);
        }
        const theaterAsBytes = await ctx.stub.getState(id); // get the car from chaincode state
        if (!theaterAsBytes || theaterAsBytes.length === 0) {
            throw new Error(`$ Theater with id ${id} does not exist`);
        }
        console.info(theaterAsBytes.toString());
        return theaterAsBytes.toString();
    }


    async getMovie(ctx, id) {
        if (id.length <= 0) {
            throw new Error(`Please provide id param!`);
        }
        const movieAsBytes = await ctx.stub.getState(id); // get the car from chaincode state
        if (!movieAsBytes || movieAsBytes.length === 0) {
            throw new Error(`$ Movie with id ${id} does not exist`);
        }
        console.info(movieAsBytes.toString());
        return movieAsBytes.toString();
    }


    async getShow(ctx, id) {
        if (id.length <= 0) {
            throw new Error(`Please provide id param!`);
        }
        const showAsBytes = await ctx.stub.getState(id); // get the car from chaincode state
        if (!showAsBytes || showAsBytes.length === 0) {
            throw new Error(`$ Show with id ${id} does not exist`);
        }
        console.info(showAsBytes.toString());
        return showAsBytes.toString();
    }

    async getPurchaseDetails(ctx, id) {
        if (id.length <= 0) {
            throw new Error(`Please provide id param!`);
        }
        const PurchaseAsBytes = await ctx.stub.getState(id); // get the car from chaincode state

        console.info("-getPurchaseDetails--PurchaseAsBytes---",PurchaseAsBytes)
        if (!PurchaseAsBytes || PurchaseAsBytes.length === 0) {
            throw new Error(`Purchase with id ${id} does not exist`);
        }
        console.info(PurchaseAsBytes.toString());
        return PurchaseAsBytes.toString();
    }


    //@TODO add only theater Owner can add a check .
    async addTheater(ctx, id, name, max_window_count, max_movie_count) {

        if (max_window_count.length <= 0) {
            throw new Error(`Please provide non-zero max_window_count!`);
        }

        if (max_movie_count.length <= 0) {
            throw new Error(`Please provide max_movie_count param!`);
        }

        if (name.length <= 0) {
            throw new Error(`Please provide name param!`);
        }

        if (id.length <= 0) {
            throw new Error(`Please provide id param!`);
        }

        console.info('============= START : addTheater  ===========', id);
        const theaterAsBytes = await ctx.stub.getState(id); // get the Theater from chaincode state
        console.info('============= theaterAsBytes ===========', theaterAsBytes);

        if (theaterAsBytes && theaterAsBytes.length > 0) {
            throw new Error(`Theater ${id} already exist`);
        }
        const theater = {
            id,
            name,
            max_window_count,
            max_movie_count
        };

        await ctx.stub.putState(id, Buffer.from(JSON.stringify(theater)));
        console.info('============= END : addTheater ===========');
    }



    //@TODO  only winodw owner must be alowed to add movie .
    async addMovie(ctx, id, name, theaterId, max_show_count) {
        console.info('============= START : addMovie  ===========', id);

        const exists = await this.getTheater(ctx, theaterId);
        console.info("--addMovie :exists -", exists)
        if (!exists) {
            throw new Error(`Theater does exists with given ${theaterId}`);
        }
        console.info("--addMovie :name -", name)

        if (name.length <= 0) {
            throw new Error(`Please provide movie name!`);
        }

        if (id.length <= 0) {
            throw new Error(`Please provide id param!`);
        }
        //check if movieId already present.
        const movieAsBytes = await ctx.stub.getState(id); // get the Theater from chaincode state
        console.info('============= movieAsBytes ===========', movieAsBytes);

        if (movieAsBytes && movieAsBytes.length > 0) {
            throw new Error(`Movie ${id} already exist`);
        }

        if (movieAsBytes && movieAsBytes.length > 0 && movieAsBytes.name == name) {
            throw new Error(`Movie ${name} can not be same ! Name already exits !!`);
        }

        const movie = {
            id,
            name,
            theaterId,
            // Maximum no of shows that a movie will have : 4. 
            max_show_count,
            //defines the number of shows remaining for the  movie .
            //@TODO try to manage as a  key value ie date(converting date as time stamp using epochconverter with Hr Min Sec as 0 : 00 : 0   ) : reamainingshow_count
            reamainingshow_count: max_show_count
        };

        await ctx.stub.putState(id, Buffer.from(JSON.stringify(movie)));
        console.info('============= END : addMovie ===========');
    }


    //  //@TODO  only winodw owner must be alowed to add movie .
    async addShowforMovie(ctx, id, timestamp, movieId, max_ticket_count, show_tittle) {

        console.info('============= START  :addShowforMovie  ===========');
        const movieAsBytes = await this.getMovie(ctx, movieId);
        console.info('=========1==== movieAsBytes : ===========', movieAsBytes);

        if (!movieAsBytes) {
            throw new Error(`Movie does exists with given ${movieId}`);
        }
        let movie = JSON.parse(movieAsBytes.toString());
        console.info('=========2==== movie.reamainingshow_count  : ===========', movie.reamainingshow_count);


        // There must exist minimum 1 show remaing for the given movie so that this 
        //show can be added and couter will be updated/decremented  .
        if (!(movie && movie.reamainingshow_count >= 1)) {
            throw new Error(`Max limit ${movie.reamainingshow_count} for the show of movie is full-filled for day.`);
        }

        if (max_ticket_count.length <= 0) {
            throw new Error(`Please provide non-zero name!`);
        }

        if (timestamp.length <= 0) {
            throw new Error(`Please provide non-zero timestamp!`);
        }

        console.info('=======3======  : addShowforMovie  ===========', id);
        const showAsBytes = await ctx.stub.getState(id); // get the Theater from chaincode state
        console.info('==========4=== showAsBytes ===========', showAsBytes);

        if (showAsBytes && showAsBytes.length > 0) {
            throw new Error(`Show ${id} already exist`);
        }

        const show = {
            id,
            show_tittle: show_tittle,
            timestamp,
            movieId,
            max_ticket_count,
            //defines the number of unoccupied(remaining) seats for the show ie  max available no of tickets that cam be sold out.   
            //At the initial, no seats are being occuipied so reaming seats for show will be max available tickets.
            remaining_show_seat_count: max_ticket_count
        };
        console.info('============= show : ===========', show);

        await ctx.stub.putState(id, Buffer.from(JSON.stringify(show)));

        //decrement  the count of the reamainingshow_count for the movieid by 1 


        console.info('============= movie : ===========', movie);

        console.info('==========T1=== movie.reamainingshow_count : ===========', movie.reamainingshow_count);

        console.info('==========T2=== movie.reamainingshow_count : typeOf ===========', typeof movie.reamainingshow_count);


        movie.reamainingshow_count = parseInt(movie.reamainingshow_count) - 1;
        console.info('======latest Movie Details =====    movie.reamainingshow_count = : ===========', movie.reamainingshow_count);

        await ctx.stub.putState(movieId, Buffer.from(JSON.stringify(movie)));
        console.info('============= END : addShow ===========');
    }


    async purchaseTickets(ctx, id, windowId, theaterId, totalAmount, no_of_Users, showId) {

        console.info('========1===== start : purchase ===========');

        const purchaseexists = await ctx.stub.getState(id); // get the car from chaincode state

        if (purchaseexists && purchaseexists.length > 0) {
            throw new Error(`Purchase already exists with given ${id}`);
        }

        const showexists = await this.getShow(ctx, showId);

        if (!showexists) {
            throw new Error(`Show does exists with given ${showId}`);
        }
        console.info('========2===== showexists  ===========', showexists);

        let show = JSON.parse(showexists.toString());
        console.info('=======3====== show : ===========', show.remaining_show_seat_count);

        console.info('=======4===== show.remaining_show_seat_count: ===========', show.remaining_show_seat_count);

        console.info('=======5=====.no_of_Users: ===========', no_of_Users);

        if (parseInt(show.remaining_show_seat_count) < parseInt(no_of_Users)) {
            throw new Error(`Insufficient No of tickets left to accomdate all users.Seats Left : ${show.remaining_show_seat_count} `);
        }

        let startSeatno = (parseInt(show.max_ticket_count) - parseInt(show.remaining_show_seat_count)) + 1
        //Seat No is generated on basis of showId .

        let sseatInfo = "SEAT" + showId + "@" + startSeatno
        let endSeatno = no_of_Users > 1 ? parseInt(show.max_ticket_count) - parseInt(show.remaining_show_seat_count) + parseInt(no_of_Users) : startSeatno
        let eseatInfo = "SEAT" + showId + "@" + endSeatno

        const purchase = {
            id,
            ticketId : id,
            windowId,
            theaterId,
            totalAmount,
            no_of_Users,
            showId,
            startSeat: sseatInfo,
            isFreeBieCollected : false,
            isWaterBottleExchanged : false,
            endSeat: eseatInfo
        };

        await ctx.stub.putState(id, Buffer.from(JSON.stringify(purchase)));

        show.remaining_show_seat_count = parseInt(show.remaining_show_seat_count) - parseInt(no_of_Users)

        await ctx.stub.putState(showId, Buffer.from(JSON.stringify(show)));

        console.info('============= END : purchase ===========');

    }


    async collectFreeBie(ctx,purchaseId){
        const purchaseexists = await this.getPurchaseDetails(ctx, purchaseId);

        if (! purchaseexists ) {
            throw new Error(`Purchase does not exists with given ${purchaseId}`);
        } 

        let purchaseInfo = JSON.parse(purchaseexists.toString());
        if(purchaseInfo.isFreeBieCollected){
            throw new Error(`Bottle and popcorn already collected for purchaseId ${purchaseId}`);
        }
        purchaseInfo.isFreeBieCollected = true

        let luckyNo = await this.getpsudonumber(ctx);
        console.log("---luckyNo-----",luckyNo)

        purchaseInfo.luckyNo  = luckyNo 
        await ctx.stub.putState(purchaseId, Buffer.from(JSON.stringify(purchaseInfo)));
    } 


    //@TODO add only theater Owner can add a check .
    async addCafeteria(ctx,id, theaterId) {

        const exists = await this.getTheater(ctx, theaterId);
        console.info("--getTheater :exists -", exists)
        if (!exists) {
            throw new Error(`Theater does exists with given ${theaterId}`);
        }
        let theaterInfo = JSON.parse(exists.toString());

        if( theaterInfo.hasOwnProperty("cafeteriaId")){
            throw new Error(`Cafeteria already exist for given theater with id ${theaterId}`);
        }

        console.info('============= START : addCafeteria  ===========', id);
        const cafeteriaAsBytes = await ctx.stub.getState(id); // get the Cafeteria from chaincode state
        console.info('============= cafeteriaAsBytes ===========', cafeteriaAsBytes);

        if (cafeteriaAsBytes && cafeteriaAsBytes.length > 0) {
            throw new Error(`Cafeteria ${id} already exist`);
        }
        const cafeteria = {
            id,
            theaterId,
            max_soda_bottle_count : 200,
            remaining_soda_bottle_count :200
        };

        await ctx.stub.putState(id, Buffer.from(JSON.stringify(cafeteria)));

        theaterInfo.cafeteriaId = id
        await ctx.stub.putState(theaterId, Buffer.from(JSON.stringify(theaterInfo)));

        console.info('============= END : addTheater ===========');
    }

    async getCafeteriaDetails(ctx, id) {
        if (id.length <= 0) {
            throw new Error(`Please provide id param!`);
        }
        const cafeteriaAsBytes = await ctx.stub.getState(id); // get the car from chaincode state
        if (!cafeteriaAsBytes || cafeteriaAsBytes.length === 0) {
            throw new Error(`$ Cafeteria with id ${id} does not exist`);
        }
        console.info(cafeteriaAsBytes.toString());
        return  cafeteriaAsBytes.toString();
    }

    

    async applyBottleforExchage(ctx, purchaseId) {  
        console.info("--START applyBottleforExchage -------",purchaseId)
        const purchaseexists = await this.getPurchaseDetails(ctx, purchaseId);
        console.info("--applyBottleforExchage--purchaseexists---",purchaseexists)
        if (!( purchaseexists && purchaseexists.length > 0)) {
            throw new Error(`Purchase does not exists with given ${purchaseId}`);
        }

        let purchaseInfo = JSON.parse(purchaseexists.toString());

        console.info("--applyBottleforExchage--purchaseInfo---",purchaseInfo)


        if(! purchaseInfo.isFreeBieCollected){
            throw new Error(`Bottle and popcorn are not yet colected , please  collected via collectFreeBie method.`);
        }

        const theaterExists = await this.getTheater(ctx, purchaseInfo.theaterId)
        let theaterInfo = JSON.parse(theaterExists.toString());

        console.info("--applyBottleforExchage--theaterInfo---",theaterInfo)
        const cafeteriaExists =  await this.getCafeteriaDetails(ctx, theaterInfo.cafeteriaId);
        let cafeteriaInfo = JSON.parse(cafeteriaExists.toString());

        if(purchaseInfo.isWaterBottleExchanged)
        throw new Error(`Sorry you have already used the exchange service.`);

        console.info("--applyBottleforExchage--cafeteriaInfo---",cafeteriaInfo)
        let isSodaAvailable =  cafeteriaInfo.remaining_soda_bottle_count > 0 ? true : false 

        if(!isSodaAvailable)
        throw new Error(`Sorry the soda bottle are not available now. Remaining Bottle : ${cafeteriaInfo.remaining_soda_bottle_count}`);

        purchaseInfo.isWaterBottleExchanged = true

        let willGetsoda = parseInt(purchaseInfo.luckyNo) % 2 === 0 ? true : false
        console.info("--applyBottleforExchage--willGetsoda---",willGetsoda)

        if(willGetsoda){
            cafeteriaInfo.remaining_soda_bottle_count = parseInt(cafeteriaInfo.remaining_soda_bottle_count) - 1 
            console.info("--applyBottleforExchage-latest-cafeteriaInfo---",cafeteriaInfo)
            await ctx.stub.putState(cafeteriaInfo.id, Buffer.from(JSON.stringify(cafeteriaInfo)));

        }
        await ctx.stub.putState(purchaseInfo.id, Buffer.from(JSON.stringify(purchaseInfo)));

     }




}

module.exports = FabCar;

