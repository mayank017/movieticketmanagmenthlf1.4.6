## Problem Statement

Write a simple Movie ticket application. Make assumptions on attributes for the various objects
Four (4) ticketing windows sell movie tickets at a theatre
People can buy one or more tickets
Once 100 tickets are sold for a movie, that movie-show  is full
The theatre runs 5 movies at any time, and each show 4 times a day
Once a ticket is purchased a buyer automatically gets a bottle of water and popcorn on Window-1
At the end of the purchase, a ticket and receipt  is printed and the purchase is recorded on the blockchain
The buyer can go and exchange the water for soda at the cafeteria. Window 1 must generate a random number. If that number is even, the buyer must be able to get the water exchanged for soda at the cafeteria. The cafeteria has only 200 sodas, so only the first 200 requesters can exchange. 


## Project Overview 
This project consists of a using a hyperledger fabric based  blockchain network over development mode.The ordering service is configured with raft consensus.
Movie theater managment with functionalities of adding thearer, movies, shows , cafetria, soda exchange , purchase is been managed via help of the chainocde.

The available test use-cases information can be found in the ticket_chaincode_hf_testcases.pdf document available  inside test-case folder in repo.

## Available chain code functions 
   
getTheater: To query the theater information on basis of theater id.

getMovie:  To query the movie information on basis of movie id.

getShow : To query the show information on basis of show id.

getPurchaseDetails : To query the purchase information on basis of show id.

addTheater : Add the theater over the ledger via inputting :
                  id :  Id of theater must be unique.
                  
                  name : Name of the theater 
                  
                  max_window_count : Max no of windows for theater to sell tickets ie 4 ticketing windows as per shared problem.
                  
                  max_movie_count : Max no of movies theater can run at a time ie 5 movies


addMovie :  Add movie to theater 

                  id :  Id of movie must be unique.
                  
                  name : Name of movie 
                   
                  theaterId : Id of theater where movies will be available.
                    
                  max_show_count : Max no of shows for a day for movie ie 4 



addShowforMovie : Add the shows for the movie 
                  id :  Id for show must be unique.
                  
                  timestamp :  TIme information in epoch format 
                  
                  movieId : id of the movie for which show to be added
                  
                  max_ticket_count : Maxm no of tickets available for the show ie 100 tickets. 
                  
                  show_tittle : Tittle of show 


purchaseTickets : To buy the tickets 
                  id : Id for purchase mst be unique, 

                  windowId : Id of window from where ticket is sold ,

                  theaterId :Id of theater selling the tikcet , 

                  totalAmount :  Total Amount paid by user for purchase of tickets,

                  no_of_Users : Count of no of users watching a  novie , 

                  showId : Id of show for which tickets is sold out.


collectFreeBie : Store & Collect the water bottle and pop-corn(to be sold from winodow 1 only)

                  purchaseId :  Id of  purchase/ticket


addCafeteria : Add the cafeteria for the theater(Only 1 cafeteria is allowed for a theater)
                  id : Id of cafeteria.

                  theaterId : Id of theater.


getCafeteriaDetails :  Get the information about the cafeteiria 

                  id : : Id of cafeteria.

applyBottleforExchage : Apply/exchange the  water bottle  with soda bottle (only initial 200 requets).

      purchaseId : Id of purchase/ticket


## Assumptions

Assumption : 

1. Only a single movie will be available for a show at a time .

2  Id are termed to be unique in the system ie show/movie/theater/cafeteria/purchase .All the id's are unique.

3. Seats are allowed to users in sequencial manner.

4. For a purchase , will get only 1 watter bottle and pop corn.

## About Readme.md 

This file contains the information about the commands, dependencies, and other issues fix if they run during the setup process of your project.


## Setup the project (Commands)
The shared code has been tested over the system having versions dependencies as below(required) :

docker version:  18.03.1-ce

local binary version:  1.4.6

docker-compose version 1.18.0, build 8dd22a9


go version go1.12 Linux/amd64

Nodejs : v8.17.0

Ubuntu: 16.04 

1. To start the etc raft ordering service please run the following command : 

docker-compose -f docker-compose-orderer-etcdraft.yaml up -d 

2. To start the peer0 for Org1, please run the following command : 

docker-compose -f docker-compose-peer0-org1.yaml up -d

3. To start the peer0 for Org2,please run the following command : 

docker-compose -f docker-compose-peer0-org2.yaml up -d

4. To start the ca for Org1, please run the following command : 

docker-compose -f docker-compose-org1-ca.yaml up -d 

5. To start the ca for Org2,please run the following command : 

docker-compose -f docker-compose-ca-org2.yaml up -d
 


Now run the docker ps command over the terminal to list out all the running containers.

The below commands are required to be executed from a peer container( via the help of command docker exec -it peer_containerId bash)

peer channel create -f /opt/gopath/src/github.com/chaincode/artifacts/mychannel.tx  -c mychannel  -o orderer1.com:7050  --tls  --cafile /etc/hyperledger/orderer/msp/tlsca/tlsca.orderer-cert.pem 



Please also find the available doc for test cases for a better understanding of the above chain code.


## Issue/Error & its fix's :

Note : 

1. If we face issues like : Timeout-expired-while-starting-chaincode / api 404 network not found:
 
https://stackoverflow.com/questions/44325840/hyperledger-fabric-1-0-alpha2-timeout-expired-while-starting-chaincode

Solution:  run sudo docker network ls
copy the name of network shown and replace the value with 
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE= name_obtaied

2. If we are unable  to run the ca via docker-compose  & getting the private key mismatch issue 
when you generated a new crypto config, Then inside :

docker-compose-org1-ca.yaml
docker-compose-ca-org2.yaml
files.
Update the FABRIC_CA_SERVER_CA_KEYFILE params with the location of the new certificate. 
